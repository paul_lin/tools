#!/bin/bash

bash_aliases_path=~/.bash_aliases

append_alias_setting() {
    echo "alias ssh_vpn_target='bash ~/tools/ssh_vpn_target.sh'" >> $bash_aliases_path
    echo "alias deploy_vim_config='bash ~/tools/deploy_vim_conf.sh'" >> $bash_aliases_path
    echo "alias cd_xpclient_dir='cd /var/www/odev_api/web/xmightgw/'" >> $bash_aliases_path
    echo "alias du_sort_by_size='du -shx ./* | sort -hr | head -n 10'" >> $bash_aliases_path
    echo "alias compile_to_pyc='bash ~/tools/compile_to_pyc.sh'" >> $bash_aliases_path
    echo "alias scp_to_target='bash ~/tools/scp_to_target.sh'" >> $bash_aliases_path
}

if [[ -f $bash_aliases_path ]]; then
    ans=''
    read -p '.bash_aliases 已存在，確認要附加上去嗎？ [y/n] ' ans
    if [[ $ans == 'y' ]]; then
        append_alias_setting
    fi
    exit 0
else
    touch ~/.bash_aliases
    append_alias_setting
fi
source ~/.bash_aliases

