#!/bin/bash

debug_project_path='/home/paul_lin/dev/ems_gateway_debug'

if [[ ! -d $debug_project_path ]]; then
    mkdir $debug_project_path
elif [[ "$(ls -A $debug_project_path)" ]]; then
    echo "xpclient debug build is already exist: $debug_project_path" 
    echo -e
    
    ans=''
    read -p 'you want to re-create it? [y/n]' ans
    if [[ $ans == 'y' ]]; then
        echo "remove old files in $debug_project_path"
        rm -rf $debug_project_path/*
        rm -rf $debug_project_path/.git
        rm -rf $debug_project_path/.gitignore
        rm -rf $debug_project_path/.gradle
        echo -e
    else
        exit 0
    fi
fi

echo "create debug project in $debug_project_path"
git clone -b feature_ocpp --single-branch https://kaze_@bitbucket.org/xmightinc/ems_gateway.git $debug_project_path
echo -e

if [[ "$(ls -A $debug_project_path)" ]]; then
    echo 'set xpclient using the test version Kaa server'
    cp ~/tools/debug_build.gradle $debug_project_path/build.gradle
    echo -e
fi

echo "記得要把所有的 .sh 改成執行 .py 而不是 .pyc"
echo -e

ans=''
read -p 'you want to build it now? [y/n]' ans
if [[ $ans == 'y' ]]; then
    cd $debug_project_path
    ./gradlew 
else
    exit 0
fi

