#!/bin/bash

is_read_from_config=true
config=''
if [[ $is_read_from_config == true ]] && [[ -f ~/tools/compile_to_pyc.conf ]]; then
    readarray -t config <~/tools/compile_to_pyc.conf
else
    is_read_from_config=false
fi

target_python_file=''
if [[ $is_read_from_config == true ]]; then
    target_python_file=${config[0]}
else
    read -p 'Target Python File? (include path) ' target_python_file
fi
target_python_dir=$(dirname $target_python_file)

# 清理舊的 .pyc 檔案
py3clean $target_python_dir
# 編譯
python3 -m py_compile $target_python_file

ans=''
read -p 'Need to move .pyc to target machine? [y/n] ' ans
if [[ $ans == 'y' ]]; then
    target_ip=''
    if [[ $is_read_from_config == true ]]; then
        target_ip=${config[1]}
    else
        read -p 'Enter target ip ' target_ip
    fi

    target_path=''
    if [[ $is_read_from_config == true ]]; then
        target_path=${config[2]}
    else
        read -p 'Enter target path ' target_path
    fi
    target_path=${target_path%/}

    new_name=''
    if [[ $is_read_from_config == true ]]; then
        new_name=${config[3]}
    else
        read -p 'Enter new full file name for target machine (null mean no change) ' new_name
    fi

    if [[ ! -z $new_name ]]; then
        # 檔名不變
        new_name=$(basename $target_python_file | cut -f 1 -d '.')
    else
        # 把新檔名中的副檔名移除
        new_name=$(echo $new_name | cut -f 1 -d '.')
    fi

    scp -P 9980 $target_python_dir/__pycache__/$new_name*.pyc xmight@$target_ip:$target_path/$new_name.pyc
fi

