#!/bin/bash
conf_name=""
read -p "Enter config name " conf_name

cd /opt/openvpn-ca/
source vars
./build-key $conf_name
cd ./client-configs
./make_config.sh $conf_name
cp ./files/$conf_name.ovpn /home/paul_lin/vpn_conf/
echo "VPN config file already move to ~/vpn_conf"

input_vpn_ip() {
    vpn_ip=""
    read -p "Enter usable VPN IP " vpn_ip
    cd /etc/openvpn/ccd
    if grep -q "$vpn_ip" * ; then
        echo "The IP already used."
        input_vpn_ip
    else
        echo "ifconfig-push $vpn_ip 255.255.0.0" | tee ./$conf_name
        echo "Already recorded to /etc/openvpn/ccd/$conf_name"
    fi
}
input_vpn_ip

ans=""
read -p "Need to copy config file to target? [y/n] " ans
if [[ $ans != "y" ]]; then
    exit 0
fi

target_ip=""
read -p "Enter target IP " target_ip
cd /home/paul_lin/vpn_conf
scp -P 9980 ./$conf_name.ovpn xmight@$target_ip:~/$conf_name.conf
echo Already copy file to ~/$conf_name.conf
#ssh -p 9980 $target_ip -t "sudo mv ~/$conf_name /etc/openvpn/$conf_name.conf"

