import serial
import argparse
import time
from datetime import datetime
from queue import Queue
import threading


class SerialHandler():
    __serial_handler = None
    __messages = Queue()

    def __init__(self, com_port=None, baud=9600):
        if com_port is not None:
            try:
                self.__serial_handler = serial.Serial(com_port, baud)
            except serial.serialutil.SerialException as err:
                print('COM Port: {} 無法開啟，發生錯誤: {}'.format(com_port, err))
                raise

    def start_receiver(self):
        receiver = threading.Thread(target=self.__receive_data, name='charger_message_receiver')
        print('start receiver thread')
        receiver.start()

    def send_msg(self, message: bytearray):
        if not isinstance(message, bytearray):
            print('unsupported data type: {}'.format(type(message)))
            raise ValueError

        checksum = 0
        for i in range(1, len(message)):
            checksum += message[i]
        message.append(checksum & 0xFF)

        for i in range(0, len(message) - 1):
            if(message[i] == 0xc0):
                message[i: i + 1] = b'\xdb\xdc'
            elif(message[i] == 0xdb):
                message[i: i + 1] = b'\xdb\xdd'

        message = bytearray(b'\xc0' + message + b'\xc0')
        print('[{}] Tx: {}'.format(self.__get_now_time(), hex_str(message)))

        for _ in range(0, 2):
            time.sleep(0.1)
            self.__serial_handler.write(message)

    def __receive_data(self):
        response_data = bytearray()
        last_received_time = time.time()
        while True:
            time.sleep(0.2)
            data_length = self.__serial_handler.in_waiting
            if not data_length and time.time() - last_received_time >= 30:
                print('can not read any messages from slaves')

            try:
                received_data = self.__serial_handler.read(data_length)
            except (serial.SerialException, serial.SerialTimeoutException) as err:
                print('can not read data from serial port, error: {}'.format(err))
                continue
            response_data += received_data

            end_index = response_data.find(b'\xC0')
            if end_index == -1:
                print('can not found C0 byte')
                continue

            message = response_data[:end_index + 1]
            self.__messages.put(message)
            last_received_time = time.time()
            response_data = response_data[end_index + 1:]
            self.__process_messages()

    def __process_messages(self):
        while not self.__messages.empty():
            response_data = self.__messages.get()
            response_data = self.__get_unpacked_message(response_data)
            if len(response_data) >= 14 and self.__validate_message(response_data):
                print('[{}] RX Unpacked Message: {}'.format(self.__get_now_time(), hex_str(response_data)))
            self.__messages.task_done()

    def __get_unpacked_message(self, message: bytearray) -> bytearray:
        # 移除 0xC0 byte
        identify_byte_index = message.find(b'\xC0')
        message = message[:identify_byte_index]

        # 還原保留字
        new_message = message.copy()
        for i in range(len(message) - 2):
            if message[i] == 0xdb:
                if message[i + 1] == 0xdc:
                    new_message[i: i + 2] = [0xc0]
                elif message[i + 1] == 0xdd:
                    new_message[i: i + 2] = [0xdb]
                else:
                    unlegal_char = message[i + 1]
                    print('escape character 0xDB followed by invalid character: {}'.format(hex_str(unlegal_char)))
                    new_message = bytearray()
        return new_message

    def __validate_message(self, message: bytearray) -> bool:
        if not message:
            return False

        expected_checksum = message[len(message) - 1]
        checksum = 0
        for i in range(1, len(message) - 1):
            checksum += message[i]
        checksum = checksum & 0xFF

        if(checksum != expected_checksum):
            print('checksum does not match: {}'.format(hex_str(message)))
            return False
        return True

    def __get_now_time(self):
        return get_isoformat_datetime(time.time())


def hex_str(byte_data: bytearray):
    if byte_data is None:
        return None
    return " ".join("{:02X}".format(current_byte) for current_byte in byte_data)


def get_isoformat_datetime(timestamp):
    """
    把 timestamp 轉換成 ISO-8601 格式

    Args:
        timestamp (str): 要轉換的 timestamp

    Returns:
        str: ISO-8601 格式的時間字串
    """
    if timestamp is None:
        print('timestamp is None.')
        return None

    try:
        new_datetime = datetime.fromtimestamp(timestamp)
    except OverflowError as err:
        print('get_isoformat_datetime error: {}'.format(err))
    return datetime.isoformat(new_datetime)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--message', nargs='*',
                        help='Command and Data Body. e.g. FB 1B 77 77 09 88 00 00 00 00 00 00 00 00 00', required=True)
    parser.add_argument('-p', '--com_port', default='/dev/ttymxc4',
                        help='Communication port of tesla charger. e.g. /dev/ttymxc4', required=True)
    args = parser.parse_args()

    serial_port = args.com_port
    serial_handler = SerialHandler(serial_port)
    print('use serial port: {}'.format(serial_port))

    serial_handler.start_receiver()
    time.sleep(1)

    # 從 list 轉成 str，再轉成 bytearray
    messages = ''.join(args.message)
    messages = bytearray(bytes.fromhex(messages))
    serial_handler.send_msg(messages)
