#!/bin/bash
# For customming brightness with openbox keybinding.
# openbox config path: ~/.config/openox/lubuntu-rc.xml

if [ $# -eq 0 ]; then exit 1; fi

bright_file="/sys/class/backlight/intel_backlight/brightness"
max_bright_file="/sys/class/backlight/intel_backlight/max_brightness"

while read line; do
    brightness=$line
done < <(cat "$bright_file")

while read line; do
    max_brightness=$line
done < <(cat "$max_bright_file")

step=15
if [ $# -gt 1 ]; then
    step=$2
fi

declare -i brightness
if [ $1 = "-" ]; then
    if [ $brightness -ne 0 ]; then
        brightness=$brightness-$step;
        echo $brightness | sudo tee $bright_file
    fi
else
    if [ $brightness -ne $max_brightness ]; then
        brightness=$brightness+$step;
        echo $brightness | sudo tee $bright_file
    fi
fi

