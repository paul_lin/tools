#!/bin/bash

start_ip=''
end_ip=''

read -p 'Please Enter Start IP ' start_ip
read -p 'Please Enter End IP ' end_ip

start_ip=$(echo $start_ip | cut -d '.' -f 4)
end_ip=$(echo $end_ip | cut -d '.' -f 4)

if [[ ! -d ~/xpclient_logs ]]; then
    mkdir ~/xpclient_logs
fi

for i in $(seq $start_ip $end_ip); do
    if [[ ! -d ~/xpclient_logs/10.8.0.$i ]]; then
        mkdir ~/xpclient_logs/10.8.0.$i
    fi

    echo copy from 10.8.0.$i:
    if [[ $1 == 'all' ]]; then
        scp -o ConnectTimeout=10 -P 9980 xmight@10.8.0.$i:~/Desktop/xmight/*log* ~/xpclient_logs/10.8.0.$i/
    else
        scp -o ConnectTimeout=10 -P 9980 xmight@10.8.0.$i:~/Desktop/xmight/xpclient*.log ~/xpclient_logs/10.8.0.$i/
    fi
done

