#!/bin/bash

target=""

if [[ ! -z $1 ]]; then
    ssh -p 9980 xmight@$1
else
    read -p "Target IP? " target
    ssh -p 9980 xmight@$target
fi

