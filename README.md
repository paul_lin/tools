# 維運工具

- 目前的工具都是針對 10.8.0.xxx 的網域撰寫，之後有擴充就需要更新

- 各工具用途：

```bash
      # 用於複製目標機器的特定檔案到 ems 上
      collect_target_files.sh

      # compile_to_pyc.sh 的設定檔，可以不用一直重複輸入
      compile_to_pyc.conf.backup

      # 把 .py 編譯成 .pyc，並複製到目標機器上
      compile_to_pyc.sh

      # 建立 open vpn 的設定檔，並複製到目標機器上
      create_vpn_conf.sh

      # 建立 xpclient 的測試環境
      create_xpclient_debug_build.sh

      # create_xpclient_debug_build.sh 會用到
      debug_build.gradle

      # 把自己的 vim 設定檔複製到目標機器上
      deploy_vim_conf.sh

      # 複製目標機器的所有 log 到 ems 上
      get_logs.sh

      # vim 設定檔，deploy_vim_conf.sh 會用到
      no_plug_vim_conf.tar.gz

      # 複製檔案到目標機器上
      scp_to_target.sh

      # 透過 ssh 讓目標機器執行命令
      send_command_to_target.sh

      # 對特斯拉充電樁送 command
      send_msg_to_tesla_charger.py

      # 對維運工具設定 alias
      set_alias.sh

      # ssh 到目標機器上，不用每次都要指定 port
      ssh_vpn_target.sh
```

  - get_logs.sh
    - 用 collect_target_files.sh 也可以達到一樣的效果，只是很常用到，所以獨立出來
  - debug_build.gradle
    - 讓 xpclient 變成資料上傳到 Kaa test server
    - 不把 .py 編譯成 .pyc，直接執行，要注意正式機都只能使用 .pyc
  - send_command_to_target.sh
    - 不能執行需要 root 權限的命令
  - send_msg_to_tesla_charger.py
    - 目前有 bug，回傳的資料會有多餘的部份