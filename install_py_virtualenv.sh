#!/bin/bash

#sudo apt install default-libmysqlclient-dev
#pip install mysqlclient

mkvirtualenv iot_dev -p /usr/bin/python3.6
pip install pylint rope flake8 autopep8
pip install requests psycopg2 PyMySQL
pip install pyserial azure-iot-hub azure-eventhub azure-iot-device

mkvirtualenv web_dev -p /usr/bin/python3.6
pip install pylint rope flake8
pip install requests psycopg2 PyMySQL
pip install Django django-env

mkvirtualenv crawler_dev -p /usr/bin/python3.6
pip install pylint rope flake8
pip install requests psycopg2 PyMySQL
pip install Scrapy selenium beautifulsoup4

