#!/bin/bash

start_ip=''
end_ip=''
target_path=''

read -p 'Please Enter Start IP ' start_ip
read -p 'Please Enter End IP ' end_ip
read -p 'Please Enter Target File Path ' target_path

start_ip=$(echo $start_ip | cut -d '.' -f 4)
end_ip=$(echo $end_ip | cut -d '.' -f 4)

if [[ ! -d ~/collected_files ]]; then
    mkdir ~/collected_files
fi

for i in $(seq $start_ip $end_ip); do
    if [[ ! -d ~/collected_files/10.8.0.$i ]]; then
        mkdir ~/collected_files/10.8.0.$i
    fi

    echo copy from 10.8.0.$i:
    scp -o ConnectTimeout=10 -P 9980 xmight@10.8.0.$i:$target_path ~/collected_files/10.8.0.$i/
done

