#!/bin/bash

start_ip=''
end_ip=''
cmd=''
read -p 'Please Enter Start IP ' start_ip
read -p 'Please Enter End IP ' end_ip
read -p 'Please Enter command ' cmd

start_ip=$(echo $start_ip | cut -d '.' -f 4)
end_ip=$(echo $end_ip | cut -d '.' -f 4)

for i in $(seq $start_ip $end_ip); do
    ssh -p 9980 xmight@10.8.0.$i -t "$cmd"
done

