#!/bin/bash

start_ip=''
end_ip=''
local_files=''
target_path=''
read -p 'Please Enter Start IP ' start_ip
read -p 'Please Enter End IP ' end_ip
read -p 'Please Enter file full path ' local_files
read -p 'Please Enter target location ' target_path

start_ip=$(echo $start_ip | cut -d '.' -f 4)
end_ip=$(echo $end_ip | cut -d '.' -f 4)

for i in $(seq $start_ip $end_ip); do
    scp -P 9980 $local_files xmight@10.8.0.$i:$target_path
done

